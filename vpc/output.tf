output "id" {
  value = google_compute_network.vpc_network.id
}

output "networkname" {
  value = google_compute_network.vpc_network.name
}
