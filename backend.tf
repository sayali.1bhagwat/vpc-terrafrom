
terraform {
  backend "gcs" {
    bucket  = "saitestterraform"
    prefix  = "terraform/state"
    credentials = "test-project-296411-314e43014bbc.json"
  }
}
