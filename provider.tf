provider "google" {

  credentials = file("test-project-296411-314e43014bbc.json")

  project = "test-project-296411"
  region  = "us-central1"
  zone    = "us-central1-c"
}